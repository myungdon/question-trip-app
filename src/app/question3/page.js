
'use client'
import { useState } from "react";
import { useRouter, useSearchParams } from "next/navigation";

export default function Question3() {
    const [question3, setQuestion3] = useState(0)
    const router = useRouter()
    const searchParams = useSearchParams()
    const score = Number(searchParams.get('score')) + Number(question3)

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/question4?score=${score}`)
    }

    return (
        <div className="text-center mt-52">
            <h1>질문3</h1>
            <p>파티나 모임에서 가장 즐기는 활동은 무엇인가요?</p>
            <form onSubmit={handleSubmit}>
                <label>
                    <input type="radio" value={question3} name="type" onChange={(e) => setQuestion3(10)}/>a) 모두와 대화하고 새로운 친구를 사귀는 것
                    <br/>
                    <input type="radio" value={question3} name="type" onChange={(e) => setQuestion3(7)}/>b) 소규모 그룹으로 대화하는 것
                    <br/>
                    <input type="radio" value={question3} name="type" onChange={(e) => setQuestion3(5)}/>c) 구석에서 혼자 휴식을 취하는 것
                </label>
                <br/>
                <button type="submit" className="mt-20">다음</button>
            </form>
        </div>
    )
}
