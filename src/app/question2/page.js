'use client'
import { useState } from "react";
import { useRouter, useSearchParams } from "next/navigation";

export default function Question2() {
    const [question2, setQuestion2] = useState(0)
    const router = useRouter()
    const searchParams = useSearchParams()
    const score = Number(searchParams.get('score')) + Number(question2)

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/question3?score=${score}`)
    }

    return (
        <div className="text-center mt-52">
            <h1>질문2</h1>
            <p>새로운 사람을 만날 때 어떻게 행동하나요?</p>
            <form onSubmit={handleSubmit}>
                <label>
                    <input type="radio" value={question2} name="type" onChange={(e) => setQuestion2(10)}/>a) 적극적으로 말을 걸고
                    친해지려고 해요.
                    <br/>
                    <input type="radio" value={question2} name="type" onChange={(e) => setQuestion2(7)}/>b) 상황을 보면서 적절히
                    대화에 참여해요.
                    <br/>
                    <input type="radio" value={question2} name="type" onChange={(e) => setQuestion2(5)}/>c) 상대방이 먼저 말을 걸
                    때까지 기다려요.
                </label>
                <br/>
                <button type="submit" className="mt-20">다음</button>
            </form>
        </div>
    )
}
