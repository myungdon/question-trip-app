
'use client'
import { useState } from "react";
import { useRouter, useSearchParams } from "next/navigation";

export default function Question6() {
    const [question6, setQuestion6] = useState(0)
    const router = useRouter()
    const searchParams = useSearchParams()
    const score = Number(searchParams.get('score')) + Number(question6)

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/question7?score=${score}`)
    }

    return (
        <div className="text-center mt-52">
            <h1>질문6</h1>
            <p>새로운 취미나 활동을 시작할 때 어떤 편인가요?</p>
            <form onSubmit={handleSubmit}>
                <label>
                    <input type="radio" value={question6} name="type" onChange={(e) => setQuestion6(10)}/>a)  주도적으로 그룹을 찾아 함께 시작해요.
                    <br/>
                    <input type="radio" value={question6} name="type" onChange={(e) => setQuestion6(7)}/>b) 친구들과 함께 시작해요.
                    <br/>
                    <input type="radio" value={question6} name="type" onChange={(e) => setQuestion6(5)}/>c) 혼자서 조용히 시작해요.
                </label>
                <br/>
                <button type="submit" className="mt-20">다음</button>
            </form>
        </div>
    )
}
