
'use client'
import { useState } from "react";
import { useRouter, useSearchParams } from "next/navigation";

export default function Question4() {
    const [question4, setQuestion4] = useState(0)
    const router = useRouter()
    const searchParams = useSearchParams()
    const score = Number(searchParams.get('score')) + Number(question4)

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/question5?score=${score}`)
    }

    return (
        <div className="text-center mt-52">
            <h1>질문4</h1>
            <p>단체 활동을 할 때 당신의 태도는 어떤가요?</p>
            <form onSubmit={handleSubmit}>
                <label>
                    <input type="radio" value={question4} name="type" onChange={(e) => setQuestion4(10)}/>a) 항상 앞장서서 리드하려고 해요.
                    <br/>
                    <input type="radio" value={question4} name="type" onChange={(e) => setQuestion4(7)}/>b) 필요한 경우 협력하고 돕는 편이에요.
                    <br/>
                    <input type="radio" value={question4} name="type" onChange={(e) => setQuestion4(5)}/>c) 지시를 받는 대로 따르는 편이에요.
                </label>
                <br/>
                <button type="submit" className="mt-20">다음</button>
            </form>
        </div>
    )
}
