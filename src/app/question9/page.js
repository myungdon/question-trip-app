
'use client'
import { useState } from "react";
import { useRouter, useSearchParams } from "next/navigation";

export default function Question9() {
    const [question9, setQuestion9] = useState(0)
    const router = useRouter()
    const searchParams = useSearchParams()
    const score = Number(searchParams.get('score')) + Number(question9)

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/question10?score=${score}`)
    }

    return (
        <div className="text-center mt-52">
            <h1>질문9</h1>
            <p>사람들과의 관계를 유지하는 방식은 어떤가요?</p>
            <form onSubmit={handleSubmit}>
                <label>
                    <input type="radio" value={question9} name="type" onChange={(e) => setQuestion9(10)}/>a) 자주 연락하고 만나요.
                    <br/>
                    <input type="radio" value={question9} name="type" onChange={(e) => setQuestion9(7)}/>b) 필요할 때만 연락해요.
                    <br/>
                    <input type="radio" value={question9} name="type" onChange={(e) => setQuestion9(5)}/>c) 거의 연락하지 않아요.
                </label>
                <br/>
                <button type="submit" className="mt-20">다음</button>
            </form>
        </div>
    )
}
