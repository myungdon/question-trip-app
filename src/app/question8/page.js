
'use client'
import { useState } from "react";
import { useRouter, useSearchParams } from "next/navigation";

export default function Question8() {
    const [question8, setQuestion8] = useState(0)
    const router = useRouter()
    const searchParams = useSearchParams()
    const score = Number(searchParams.get('score')) + Number(question8)

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/question9?score=${score}`)
    }

    return (
        <div className="text-center mt-52">
            <h1>질문8</h1>
            <p>모임에서의 당신의 에너지 레벨은 어떤가요?</p>
            <form onSubmit={handleSubmit}>
                <label>
                    <input type="radio" value={question8} name="type" onChange={(e) => setQuestion8(10)}/>a) 끝까지 활기차게 유지돼요.
                    <br/>
                    <input type="radio" value={question8} name="type" onChange={(e) => setQuestion8(7)}/>b) 중간쯤 되면 조금 지쳐요.
                    <br/>
                    <input type="radio" value={question8} name="type" onChange={(e) => setQuestion8(5)}/>c) 금방 지쳐서 빨리 집에 가고 싶어요.
                </label>
                <br/>
                <button type="submit" className="mt-20">다음</button>
            </form>
        </div>
    )
}
