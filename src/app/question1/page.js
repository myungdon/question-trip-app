'use client'
import { useState } from "react";
import { useRouter } from "next/navigation";

export default function Question1() {
    const [score, setScore] = useState(0)
    const router = useRouter()

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/question2?score=${score}`)
    }

    return (
        <div className="text-center mt-52">
            <h1>질문1</h1>
            <p>친구와의 모임에서 당신의 역할은 무엇인가요?</p>
            <form onSubmit={handleSubmit}>
                <label>
                    <input type="radio" value={score} name="type" onChange={(e) => setScore(10)}/>a) 항상 분위기를
                    주도하는 편이에요.
                    <br/>
                    <input type="radio" value={score} name="type" onChange={(e) => setScore(7)}/>b) 필요한 경우 분위기를
                    맞추는 편이에요.
                    <br/>
                    <input type="radio" value={score} name="type" onChange={(e) => setScore(5)}/>c) 조용히 다른 사람들의
                    이야기를 듣는 편이에요.
                </label>
                <br/>
                <button type="submit" className="mt-20">다음</button>
            </form>
        </div>
    )
}
