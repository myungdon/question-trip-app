'use client'
import { useState } from "react";
import { useRouter, useSearchParams } from "next/navigation";

export default function Question10() {
    const [question10, setQuestion10] = useState(0)
    const router = useRouter()
    const searchParams = useSearchParams()
    const score = Number(searchParams.get('score')) + Number(question10)

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/result?score=${score}`)
    }

    return (
        <div className="text-center mt-52">
            <h1>질문10</h1>
            <p>친구들과 여행을 계획할 때 당신의 역할은 무엇인가요?</p>
            <form onSubmit={handleSubmit}>
                <label>
                    <input type="radio" value={question10} name="type" onChange={(e) => setQuestion10(10)}/>a) 여행 계획을 세우고 주도해요.
                    <br/>
                    <input type="radio" value={question10} name="type" onChange={(e) => setQuestion10(7)}/>b) 친구들의 계획에 맞춰 협력해요.
                    <br/>
                    <input type="radio" value={question10} name="type" onChange={(e) => setQuestion10(5)}/>c) 친구들이 세운 계획을 따르는 편이에요.
                </label>
                <br/>
                <button type="submit" className="mt-20">다음</button>
            </form>
        </div>
    )
}
