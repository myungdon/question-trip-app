import Image from "next/image";

export default function Home() {
    return (
        <div className="text-center">
            <h1 className="text-5xl mt-72 mb-20">인싸력 테스트</h1>
            <br/>
            <a
                href="http://localhost:3000/question1">시작하기
            </a>
        </div>
    );
}
