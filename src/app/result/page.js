'use client'
import {useSearchParams} from "next/navigation";

export default function Result() {
    const searchParams = useSearchParams()
    const score = searchParams.get('score')

    return (
        <div className="text-center mt-52">
            <p className="mb-20">당신의 인싸력은 {score}점 입니다</p>
            <a href="http://localhost:3000">처음으로</a>
        </div>
    )
}