
'use client'
import { useState } from "react";
import { useRouter, useSearchParams } from "next/navigation";

export default function Question7() {
    const [question7, setQuestion7] = useState(0)
    const router = useRouter()
    const searchParams = useSearchParams()
    const score = Number(searchParams.get('score')) + Number(question7)

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/question8?score=${score}`)
    }

    return (
        <div className="text-center mt-52">
            <h1>질문7</h1>
            <p>친구들과의 대화에서 주로 어떤 주제를 이야기하나요?</p>
            <form onSubmit={handleSubmit}>
                <label>
                    <input type="radio" value={question7} name="type" onChange={(e) => setQuestion7(10)}/>a) 최신 트렌드와 인기 주제
                    <br/>
                    <input type="radio" value={question7} name="type" onChange={(e) => setQuestion7(7)}/>b) 공통 관심사
                    <br/>
                    <input type="radio" value={question7} name="type" onChange={(e) => setQuestion7(5)}/>c) 개인적인 이야기나 생각
                </label>
                <br/>
                <button type="submit" className="mt-20">다음</button>
            </form>
        </div>
    )
}
