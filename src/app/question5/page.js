
'use client'
import { useState } from "react";
import { useRouter, useSearchParams } from "next/navigation";

export default function Question5() {
    const [question5, setQuestion5] = useState(0)
    const router = useRouter()
    const searchParams = useSearchParams()
    const score = Number(searchParams.get('score')) + Number(question5)

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/question6?score=${score}`)
    }

    return (
        <div className="text-center mt-52">
            <h1>질문5</h1>
            <p>SNS를 얼마나 자주 사용하나요?</p>
            <form onSubmit={handleSubmit}>
                <label>
                    <input type="radio" value={question5} name="type" onChange={(e) => setQuestion5(10)}/>a)  매일 여러 번 확인하고 게시물도 자주 올려요.
                    <br/>
                    <input type="radio" value={question5} name="type" onChange={(e) => setQuestion5(7)}/>b) 가끔 확인하고 필요할 때만 게시물을 올려요.
                    <br/>
                    <input type="radio" value={question5} name="type" onChange={(e) => setQuestion5(5)}/>c) 거의 사용하지 않아요.
                </label>
                <br/>
                <button type="submit" className="mt-20">다음</button>
            </form>
        </div>
    )
}
